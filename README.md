# Surcoll——Survive Your College

🔨 This Page is still in developing...

> High integration and convenience college tool kit with smaller size.

![npm version](https://img.shields.io/badge/NPM%20Version-6.13.7-9cf?logo=npm) ![node version](https://img.shields.io/badge/NodeJS%20Version-12.14.1-026e00?logo=node.js) ![vuecli version](https://img.shields.io/badge/Vue%20Version-4.3.1-42b983?logo=vue.js) ![eslint version](https://img.shields.io/badge/ESLint%20Version-6.8.0-informational?logo=eslint)



## Why Surcoll?

My original intension was to make college life more efficient with less effort and less APPs on your phone. So here I proudly introduce: **Surcoll**.

Surcoll has smaller size and high level of integration. It contains most of the tools for what college student really need: Financial analysis, notes, class schedule...

However, you wouldn't choose Surcoll just for those. There's a more attractive function: **Data visualization analysis**. All your data will be managed to chart. And based on these charts, Surcoll will give you some advice.

### PROs

- 🧊 Higher integration, less APPs required (Just Surcoll)
- 🧼 Made with **Vue.js**. Faster and less memory usage (Used NPM Pack)
- 🧫 Powerful data visualization (Powered by Python)
- 🧱 No need to Internet connection at all (An outline APP)



## UI Style

![vuetify version](https://img.shields.io/badge/Vuetify%20Version-2.3.4%20LTS-1867c0?logo=vuetify) 

Surcoll was designed with Material Style. We choose [Vuetify](https://vuetifyjs.com/zh-Hans/) as its UI Framework which has powerful and smooth animation transition.

The UI components is highly customizable so that Surcoll is not just using simple colors. We also choose some faded-able images to embed in components.

We try to make it look nice and clean, and we'll make some tweaks to the UI later.



## Credits

Thanks to my best friend [Zehan Wang](https://gitee.com/youth_evening_magazine), for helped me finished some development of views.











