import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '../views/Dashboard.vue'
import Schedule from '../views/Schedule'
import Notes from '../views/Notes'
import Financial from '../views/Financial'
import Todo from '../views/Todo'
import Microtools from '../views/Microtools'
import Settings from '../views/Settings'
import Donate from '../views/Donate'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/schedule',
    name: 'Schedule',
    component: Schedule
  },
  {
    path: '/notes',
    name: 'Notes',
    component: Notes
  },
  {
    path: '/financial',
    name: 'Financial',
    component: Financial
  },
  {
    path: '/todo',
    name: 'Todo',
    component: Todo
  },
  {
    path: '/microtools',
    name: 'Microtools',
    component: Microtools
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  {
    path: '/donate',
    name: 'Donate',
    component: Donate
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = new VueRouter({
  routes
})

export default router
